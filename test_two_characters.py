import unittest
from ex import two_characters


class TWO_CHARACTERS(unittest.TestCase):
    def test_give_asvkugfiugsalddlasguifgukvsa_should_0(self):
        s = "asvkugfiugsalddlasguifgukvsa"
        expected_result = 0

        result = two_characters.two_character(s)

        self.assertEqual(expected_result, result)

    def test_give_ab_should_2(self):
        s = "ab"
        expected_result = 2

        result = two_characters.two_character(s)

        self.assertEqual(expected_result, result)

    def test_give_beabeefeab_should_5(self):
        s = "beabeefeab"
        expected_result = 5

        result = two_characters.two_character(s)

        self.assertEqual(expected_result, result)

    def test_give_pvmaigytciycvjdhovwiouxxylkxjjyzrcdrbmokyqvsradegswrezhtdyrsyhg_should_6(self):
        s = "pvmaigytciycvjdhovwiouxxylkxjjyzrcdrbmokyqvsradegswrezhtdyrsyhg"
        expected_result = 6

        result = two_characters.two_character(s)

        self.assertEqual(expected_result, result)


    def test_give_cobmjdczpffbxputsaqrwnfcweuothoygvlzugazulgjdbdbarnlffzpogdprjxvtvbmxjujeubiofecvmjmxvofejdvovtjulhhfyadr_should_8(self):
        s = "cobmjdczpffbxputsaqrwnfcweuothoygvlzugazulgjdbdbarnlffzpogdprjxvtvbmxjujeubiofecvmjmxvofejdvovtjulhhfyadr"
        expected_result = 8

        result = two_characters.two_character(s)

        self.assertEqual(expected_result, result)

    def test_give_cwomzxmuelmangtosqkgfdqvkzdnxerhravxndvomhbokqmvsfcaddgxgwtpgpqrmeoxvkkjunkbjeyteccpugbkvhljxsshpoymkryydtmfhaogepvbwmypeiqumcibjskmsrpllgbvc_should_8(self):
        s = "cwomzxmuelmangtosqkgfdqvkzdnxerhravxndvomhbokqmvsfcaddgxgwtpgpqrmeoxvkkjunkbjeyteccpugbkvhljxsshpoymkryydtmfhaogepvbwmypeiqumcibjskmsrpllgbvc"
        expected_result = 8

        result = two_characters.two_character(s)

        self.assertEqual(expected_result, result)

    def test_give_ggzczxoxwqeihjsswxwxkuxqwcyeuxrppajcyrzahehomrlqzfesagmietjqtwsrioocnjqgggpzzkmxxqlujnqormwffsmhoatqbezymyhxwjtayyilozozzwbhhkewgimejokyvsyobcfprzjdgnhwitwsdssjohhvkvgwpxcnanqufqbskubxxiajpclpraimeozhvpwagabvhjndinvuhfzqlcvbfnkosqdsdzznsibukhlltcgaezcvlsrkbexelggocseryhqyntrolpskbgojetdaidcbcbiwzsemactmumjemlqkbqyqxbgjaqynnworreqbyqyawpqyuorkoqdlvovyqkvyqxffhbfcjabcqribrikvraivghmdssjqywbtkuzhoeltouoztkdjgupiwyowglrtttgcjxnnmvkihxckbayxwcjiyyrymomqclchpiorftyuuuccymzbzobltxojsdzdrtvqwcvclsfbkvxsdcncgzpjwjwvgyxzptjjscmujoslgx_should_0(self):
        s = "ggzczxoxwqeihjsswxwxkuxqwcyeuxrppajcyrzahehomrlqzfesagmietjqtwsrioocnjqgggpzzkmxxqlujnqormwffsmhoatqbezymyhxwjtayyilozozzwbhhkewgimejokyvsyobcfprzjdgnhwitwsdssjohhvkvgwpxcnanqufqbskubxxiajpclpraimeozhvpwagabvhjndinvuhfzqlcvbfnkosqdsdzznsibukhlltcgaezcvlsrkbexelggocseryhqyntrolpskbgojetdaidcbcbiwzsemactmumjemlqkbqyqxbgjaqynnworreqbyqyawpqyuorkoqdlvovyqkvyqxffhbfcjabcqribrikvraivghmdssjqywbtkuzhoeltouoztkdjgupiwyowglrtttgcjxnnmvkihxckbayxwcjiyyrymomqclchpiorftyuuuccymzbzobltxojsdzdrtvqwcvclsfbkvxsdcncgzpjwjwvgyxzptjjscmujoslgx"
        expected_result = 0

        result = two_characters.two_character(s)

        self.assertEqual(expected_result, result)