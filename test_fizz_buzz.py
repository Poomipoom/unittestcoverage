import unittest

from ex import fizz_buzz


class FizzBuzz(unittest.TestCase):
    def test_give_3_should_Fizz(self):
        X = 3
        expected_result = "Fizz"

        result = fizz_buzz.fizzbuzz(X)

        self.assertEqual(expected_result, result)

    def test_give_5_should_Buzz(self):
        X = 5
        expected_result = "Buzz"

        result = fizz_buzz.fizzbuzz(X)

        self.assertEqual(expected_result, result)

    def test_give_15_should_Fizz(self):
        X = 15
        expected_result = "FizzBuzz"

        result = fizz_buzz.fizzbuzz(X)

        self.assertEqual(expected_result, result)
    def test_give_50_should_Buzz(self):
        X = 50
        expected_result = "Buzz"

        result = fizz_buzz.fizzbuzz(X)

        self.assertEqual(expected_result, result)

    def test_give_75_should_FizzBuzz(self):
        X = 75
        expected_result = "FizzBuzz"

        result = fizz_buzz.fizzbuzz(X)

        self.assertEqual(expected_result, result)

    def test_give_78_should_Fizz(self):
        X = 78
        expected_result = "Fizz"

        result = fizz_buzz.fizzbuzz(X)

        self.assertEqual(expected_result, result)
    
    def test_give_100_should_Buzz(self):
        X = 100
        expected_result = "Buzz"

        result = fizz_buzz.fizzbuzz(X)

        self.assertEqual(expected_result, result)