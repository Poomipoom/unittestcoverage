import unittest
from ex import grid_Challenge


class Grid_Challege(unittest.TestCase):
    def test_give_kc_iu_should_YES(self):
        grid = ["kc","iu"]
        
        expected_result = "YES"

        result = grid_Challenge.gridChallenge(grid)

        self.assertEqual(expected_result, result)

    def test_give_uxf_vof_hmp_should_NO(self):
        grid = ["uxf","vof","hmp"]
       

        expected_result = "NO"

        result = grid_Challenge.gridChallenge(grid)

        self.assertEqual(expected_result, result)

    def test_give_tjxxx_xtxxj_rzzzz_zzrzz_rzzzz_should_YES(self):
        grid = ["tjxxx","xtxxj","rzzzz","zzrzz","rzzzz"]

        expected_result = "YES"

        result = grid_Challenge.gridChallenge(grid)

        self.assertEqual(expected_result, result)

    def test_give_vpvv_pvvv_vzzp_zzyy_should_YES(self):
        grid = ["vpvv","pvvv","vzzp","zzyy"]
        
        expected_result = "YES"

        result = grid_Challenge.gridChallenge(grid)

        self.assertEqual(expected_result, result)


    def test_give_zzzzzwz_zzzzzzw_wzzzzzz_zzwzzzz_zzyzzzz_zzzzyzz_zzzzzyz_should_YES(self):
        grid = ["zzzzzwz","zzzzzzw","wzzzzzz","zzwzzzz","zzyzzzz","zzzzyzz","zzzzzyz"]
        
        expected_result = "YES"

        result = grid_Challenge.gridChallenge(grid)

        self.assertEqual(expected_result, result)

    def test_give_eibjbwsp_ptfxehaq_jxipvfga_rkefiyub_kalwfhfj_lktajiaq_srdgoros_nflvjznh_should_NO(self):
        grid = ["eibjbwsp","ptfxehaq","jxipvfga","rkefiyub","kalwfhfj","lktajiaq","srdgoros","nflvjznh"]
        
        expected_result = "NO"

        result = grid_Challenge.gridChallenge(grid)

        self.assertEqual(expected_result, result)

    def test_give_zzzzzzzzzz_zzzzzzzzzz_zzzzzzzzzz_zzzzzzzzzz_zzzzzzzzzz_zzzzzzzzzz_zzzzzzzzzz_zzzzzzzzzz_zzzzzzzzzz_zzzzzzzzzz_should_YES(self):
        grid = ["zzzzzzzzzz","zzzzzzzzzz","zzzzzzzzzz","zzzzzzzzzz","zzzzzzzzzz","zzzzzzzzzz","zzzzzzzzzz","zzzzzzzzzz","zzzzzzzzzz","zzzzzzzzzz"]

        expected_result = "YES"

        result = grid_Challenge.gridChallenge(grid)

        self.assertEqual(expected_result, result)