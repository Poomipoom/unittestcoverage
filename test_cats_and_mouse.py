import unittest

from ex import cats_mouses


class CatsAndMouse(unittest.TestCase):
    def test_give_1_2_3_should_cat_b(self):
        cat_a = 1
        cat_b = 2
        mouse_c = 3
        expected_result = "Cat B"

        result = cats_mouses.cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)
    def test_give_4_6_5_should_cat_b(self):
        cat_a = 4
        cat_b = 6
        mouse_c = 5
        expected_result = "Mouse C"

        result = cats_mouses.cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)
    def test_give_8_7_9_should_cat_b(self):
        cat_a = 8
        cat_b = 7
        mouse_c = 9
        expected_result = "Cat A"

        result = cats_mouses.cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)

    def test_give_40_50_55_should_cat_b(self):
        cat_a = 40
        cat_b = 50
        mouse_c = 55
        expected_result = "Cat B"

        result = cats_mouses.cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)

    def test_give_40_50_30_should_cat_a(self):
        cat_a = 40
        cat_b = 50
        mouse_c = 30
        expected_result = "Cat A"

        result = cats_mouses.cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)

    def test_give_100_0_50_should_mouse_c(self):
        cat_a = 100
        cat_b = 0
        mouse_c = 50
        expected_result = "Mouse C"

        result = cats_mouses.cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)
    
    def test_give_98_99_100_should_mouse_c(self):
        cat_a = 98
        cat_b = 99
        mouse_c = 100
        expected_result = "Cat B"

        result = cats_mouses.cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)
