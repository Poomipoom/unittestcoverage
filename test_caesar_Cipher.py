import unittest
from ex import caesar_Cipher


class Caesar_Cipher(unittest.TestCase):
    def test_give_middleOutz_and_1_should_njeemfPvua(self):
        s = "middleOutz"
        k = 1
        expected_result = "njeemfPvua"

        result = caesar_Cipher.caesarCipher(s,k)

        self.assertEqual(expected_result, result)

    def test_give_middleOutz_and_2_should_okffngQwvb(self):
        s = "middleOutz"
        k = 2
        expected_result = "okffngQwvb"

        result = caesar_Cipher.caesarCipher(s,k)

        self.assertEqual(expected_result, result)

    def test_give_middleOutz_and_3_should_plggohRxwc(self):
        s = "middleOutz"
        k = 3
        expected_result = "plggohRxwc"

        result = caesar_Cipher.caesarCipher(s,k)

        self.assertEqual(expected_result, result)

    def test_give_middleOutz_and_50_should_kgbbjcMsrx(self):
        s = "middleOutz"
        k = 50
        expected_result = "kgbbjcMsrx"

        result = caesar_Cipher.caesarCipher(s,k)

        self.assertEqual(expected_result, result)
        
    def test_give_middleOutz_and_98_should_gcxxfyIont(self):
        s = "middleOutz"
        k = 98
        expected_result = "gcxxfyIont"

        result = caesar_Cipher.caesarCipher(s,k)

        self.assertEqual(expected_result, result)

    def test_give_middleOutz_and_99_should_hdyygzJpou(self):
        s = "middleOutz"
        k = 99
        expected_result = "hdyygzJpou"

        result = caesar_Cipher.caesarCipher(s,k)

        self.assertEqual(expected_result, result)
    
    def test_give_middleOutz_and_100_should_iezzhaKqpv(self):
        s = "middleOutz"
        k = 100
        expected_result = "iezzhaKqpv"

        result = caesar_Cipher.caesarCipher(s,k)

        self.assertEqual(expected_result, result)
