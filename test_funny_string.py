import unittest
from ex import funny_string


class Funny_String(unittest.TestCase):
    def test_give_axcz_should_Funny(self):
        s = "axcz"
        expected_result = "Funny"

        result = funny_string.funnyString(s)

        self.assertEqual(expected_result, result)

    def test_give_bxcz_should_Not_Funny(self):
        s = "bxcz"
        expected_result = "Not Funny"

        result = funny_string.funnyString(s)

        self.assertEqual(expected_result, result)

    def test_give_ivvkx_should_Not_Funny(self):
        s = "ivvkx"
        expected_result = "Not Funny"

        result = funny_string.funnyString(s)

        self.assertEqual(expected_result, result)

    def test_give_ovyvzvptyvpvpxyztlrztsrztztqvrxtxuxq_should_Funny(self):
        s = "ovyvzvptyvpvpxyztlrztsrztztqvrxtxuxq"
        expected_result = "Funny"

        result = funny_string.funnyString(s)

        self.assertEqual(expected_result, result)

    def test_give_fmpszyvqwxrtvpuwqszvyvotmsxsxuvzyvpwzrpmuxqwtswvytytzsnuxuyrpvtysqoutzurqxury_should_Not_Funny(self):
        s = "fmpszyvqwxrtvpuwqszvyvotmsxsxuvzyvpwzrpmuxqwtswvytytzsnuxuyrpvtysqoutzurqxury"
        expected_result = "Not Funny"

        result = funny_string.funnyString(s)

        self.assertEqual(expected_result, result)
    def test_give_rjdjjhqjhkphjhfxjplmqgrlinztqwtjhvspfixnupufxycippuhunrcbrxqpqzwntnrblhwcdcybdjqtfworqrrjokzelxtelmgxsdbueyxijpcnlpfdixlwhnctkuywhfvyhvvmwstfhustlwwtvjpfvqxinwetdzrjuowqgpvmmpgwnerioimkbkrniwgkvyusnqkuhfhsvoritishseqspjhyioidoewbvhxdneujtnsmxkuponjfvpeffnisrgvmmsozxlisxcgumzlcmtgkklooyxhpecdpvrosexkdzpxnoiqdcvmnjcchzixflztgskxvjjykxxwnhmpkkdxsfybgixksydzroyoxhohgngsurke_should_Not_Funny(self):
        s = "rjdjjhqjhkphjhfxjplmqgrlinztqwtjhvspfixnupufxycippuhunrcbrxqpqzwntnrblhwcdcybdjqtfworqrrjokzelxtelmgxsdbueyxijpcnlpfdixlwhnctkuywhfvyhvvmwstfhustlwwtvjpfvqxinwetdzrjuowqgpvmmpgwnerioimkbkrniwgkvyusnqkuhfhsvoritishseqspjhyioidoewbvhxdneujtnsmxkuponjfvpeffnisrgvmmsozxlisxcgumzlcmtgkklooyxhpecdpvrosexkdzpxnoiqdcvmnjcchzixflztgskxvjjykxxwnhmpkkdxsfybgixksydzroyoxhohgngsurke"
        expected_result = "Not Funny"

        result = funny_string.funnyString(s)

        self.assertEqual(expected_result, result)
    def test_give_tuvqxpwrzxqsyumotzrxuzsxqxsvpsxzwqxzuztyrvptmrxtwrsxzstlrwyvouywzxumtlpuyzrtytyrtyvyuxzyqtwqyszwoprxytvzuvqrxzrysuvwrtuyuxyvwotltzumouxrxpwpxrxuomuztltowvyxuyutrwvusyrzxrqvuzvtyxrpowzsyqwtqyzxuyvytrytytrzyupltmuxzwyuovywrltszxsrwtxrmtpvrytzuzxqwzxspvsxqxszuxrztomuqwunltyrzsxwv_should_Funny(self):
        s = "tuvqxpwrzxqsyumotzrxuzsxqxsvpsxzwqxzuztyrvptmrxtwrsxzstlrwyvouywzxumtlpuyzrtytyrtyvyuxzyqtwqyszwoprxytvzuvqrxzrysuvwrtuyuxyvwotltzumouxrxpwpxrxuomuztltowvyxuyutrwvusyrzxrqvuzvtyxrpowzsyqwtqyzxuyvytrytytrzyupltmuxzwyuovywrltszxsrwtxrmtpvrytzuzxqwzxspvsxqxszuxrztomuqwunltyrzsxwv"
        expected_result = "Funny"

        result = funny_string.funnyString(s)

        self.assertEqual(expected_result, result)
    
