import unittest
from ex import alternating_character


class Alternating_Characters(unittest.TestCase):
    def test_give_AA_should_1(self):
        s = "AA"
        expected_result = 1

        result = alternating_character.alternatingCharacters(s)

        self.assertEqual(expected_result, result)

    def test_give_BBB_should_2(self):
        s = "BBB"
        expected_result = 2

        result = alternating_character.alternatingCharacters(s)

        self.assertEqual(expected_result, result)

    def test_give_ABABABA_should_0(self):
        s = "ABABABA"
        expected_result = 0

        result = alternating_character.alternatingCharacters(s)

        self.assertEqual(expected_result, result)

    def test_give_AAAAAAAAAAAAAAAAAAAAAAABBBBBBBBBBBBBBBBBBBBBBBBBBBBB_should_50(self):
        s = "AAAAAAAAAAAAAAAAAAAAAAABBBBBBBBBBBBBBBBBBBBBBBBBBBBB"
        expected_result = 50

        result = alternating_character.alternatingCharacters(s)

        self.assertEqual(expected_result, result)

    def test_give_AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB_should_98(self):
        s = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB"
        expected_result = 98

        result = alternating_character.alternatingCharacters(s)

        self.assertEqual(expected_result, result)

    def test_give_AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB_should_99(self):
        s = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB"
        expected_result = 99

        result = alternating_character.alternatingCharacters(s)

        self.assertEqual(expected_result, result)

    def test_give_AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB_should_100(self):
        s = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB"
        expected_result = 100

        result = alternating_character.alternatingCharacters(s)

        self.assertEqual(expected_result, result)

